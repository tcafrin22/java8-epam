package by.bsu.task6.controller;

import by.bsu.task6.comand.CommandFactory;
import by.bsu.task6.comand.ICommand;
import by.bsu.task6.comand.exception.ComandException;
import by.bsu.task6.comand.exception.CommandFactoryException;
import by.bsu.task6.resource.ConfigurationManager;
import by.bsu.task6.resource.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 03.11.2015.
 */

/**
 * This class should work with GET and POST from commandFactory
 *
 * @author Vlad
 */

public class Controller extends HttpServlet {
    private static final CommandFactory commandFactory = CommandFactory.getInstance();
    private static final String PATH_INDEX = "path.page.index";
    private static final String NULL_PAGE = "nullPage";

    private static final String WRONG_ACTION = "wrongAction";
    private static final String MESS_NULLPAGE = "message.nullpage";
    private static final String MESS_WRONG_ACTION = "message.smthwrong";
    private static final String MAIN = "main";
    private static final String STATUS = "status";
    private static final String COMMAND = "command";
    private static final Logger logger = LogManager.getLogger(Controller.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * <p>Here we calls method to define command and calls method execute of this command;If page equals null
     * we will chech if we are log in system and then send specific message to a page
     * (login or main, it depends on status of log in system);Then we go to exact page.</p>
     *
     * @param request A request that comes to this command
     * @throws ServletException it can be thrown while forwarding
     * @throws IOException      it can be thrown while forwarding
     */

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = null;

        /*
        * * вызов реализованного метода execute() и передача параметров
        * * классу-обработчику конкретной команды   */

        try {

            ICommand command = commandFactory.defineCommand(request.getParameter(COMMAND));
            page = command.execute(request);
        } catch (ComandException | CommandFactoryException e) {
            logger.error(e);
        }
        if (page != null) {
            request.getSession().removeAttribute(WRONG_ACTION);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page); //переход на страницу
            // вызов страницы ответа на запрос
            dispatcher.forward(request, response);

        } else {   // установка страницы c cообщением об ошибке
            if (request.getSession().getAttribute(STATUS) == null) {
                page = ConfigurationManager.getProperty(PATH_INDEX);
                request.getSession().setAttribute(NULL_PAGE,
                        MessageManager.getProperty(MESS_NULLPAGE));
                response.sendRedirect(request.getContextPath() + page);
            } else {
                page = MAIN;
                request.getSession().setAttribute(WRONG_ACTION,
                        MessageManager.getProperty(MESS_WRONG_ACTION));
                response.sendRedirect(request.getContextPath() + page);
            }
        }

    }
}
