package by.bsu.task6.dao;

import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.entity.Facultative;

import java.util.List;

/**
 * Created by Vlad on 16.11.2015.
 */
/**
 * <p>This interface defines behavior of SQLFacultativeDAO</p>
 *
 * @see by.bsu.task6.dao.impl.SQLFacultativeDao here the class which implements this interface
 * @author Vlad
 */
public interface FacultativeDAO {
    List<Facultative> showAllFacultative()throws DAOException;
    List<String> showSub(int studId)throws DAOException;
    void editFacultative(int facultativeId, int maxStudents, int teacherId,String name)throws DAOException;
    void deleteFacultative(int id)throws DAOException;
    void addFacultative(int facultativeId,int teacherId,int maxStudents,String name)throws DAOException;
    boolean validateOnEdit(int teacherId, int facultativeId) throws DAOException;
    boolean validateOnDelete(int id, int teacherId) throws DAOException;
    boolean validateOnAdd(int facultativeId) throws DAOException;
}
