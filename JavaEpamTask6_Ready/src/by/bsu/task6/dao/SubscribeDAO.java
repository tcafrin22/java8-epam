package by.bsu.task6.dao;

import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.entity.StudentWithMark;

import java.util.List;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This interface defines behavior of SQLSubscribeDAO</p>
 *
 * @author Vlad
 * @see by.bsu.task6.dao.impl.SQLSubscribeDAO here the class which implements this interface
 */
public interface SubscribeDAO {
    void subscribe(int facultativeId, int studentId, int maxStudents) throws DAOException;

    List<StudentWithMark> showStud(int facultativeId) throws DAOException;

    boolean validateOnSubscribe(int maxStudents, int facultativeId) throws DAOException;
}
