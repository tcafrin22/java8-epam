package by.bsu.task6.dao.exception;

/**
 * Created by Vlad on 07.11.2015.
 */

/**
 * <p>This class designed for  my own Exception- DAOException;It can be thrown in any DAO</p>
 *
 * @author Vlad
 */
public class DAOException extends Exception {
    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(Throwable exception) {
        super(exception);
    }

    public DAOException(String message, Throwable exception) {
        super(message, exception);
    }
}
