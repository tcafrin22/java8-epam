package by.bsu.task6.dao;

import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.entity.Result;

import java.util.List;

/**
 * Created by Vlad on 19.11.2015.
 */
/**
 * <p>This interface defines behavior of SQLResultDAO</p>
 *
 * @see by.bsu.task6.dao.impl.SQLResultDAO here the class which implements this interface
 * @author Vlad
 */
public interface ResultDAO {
    void addResult( int facultativeId,int studentId,int  mark,String comment,String facultativeName)throws DAOException;
    List<Result> showResult(int studentId)throws DAOException;
    boolean validateOnAddResult(int facultativeId, int studentId)throws DAOException;
}
