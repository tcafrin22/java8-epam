package by.bsu.task6.dao;

import by.bsu.task6.dao.impl.SQLFacultativeDao;
import by.bsu.task6.dao.impl.SQLResultDAO;
import by.bsu.task6.dao.impl.SQLSubscribeDAO;
import by.bsu.task6.dao.impl.SQLUserDAO;

/**
 * Created by Vlad on 07.11.2015.
 */
/**
 * <p>This class will give you an exact DAO You want</p>
 *
 * @author Vlad
 */
public class DAOFactory {
    public static DAOFactory getDAOFactory() {
        return new DAOFactory();
    }
    public UserDAO getUserDAO() {
        return new SQLUserDAO();
    }
    public FacultativeDAO getFacultativeDAO(){return new SQLFacultativeDao();}
    public SubscribeDAO getSubDAO(){return new SQLSubscribeDAO();}
    public ResultDAO getResultDAO(){return new SQLResultDAO();}
}
