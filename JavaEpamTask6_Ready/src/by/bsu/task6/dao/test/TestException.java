package by.bsu.task6.dao.test;

/**
 * Created by Vlad on 19.12.2015.
 */
public class TestException extends Exception {
    public TestException() {
    }

    public TestException(String message) {
        super(message);
    }

    public TestException(Throwable exception) {
        super(exception);
    }

    public TestException(String message, Throwable exception) {
        super(message, exception);
    }
}