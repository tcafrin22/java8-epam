package by.bsu.task6.dao.test;

import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.dao.impl.SQLUserDAO;
import by.bsu.task6.database.ConnectionPool;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Vlad on 19.12.2015.
 */
public class TestUserDAO {
    @Test
      public void testCheakUser() throws TestException {
        //один вызов на все методы
        ConnectionPool.getInstance().initializate();
        SQLUserDAO userDAO=new SQLUserDAO();
        String login="no such user in system";
        String password="no such user in system";
        Object expected=null;
        try {
            Object actualInfo=userDAO.checkUser(login,password);
            Assert.assertEquals(expected, actualInfo);
        } catch (DAOException e) {
            throw new TestException(e);
        }
    }
    @Test
    public void testValidate() throws TestException {
        SQLUserDAO userDAO=new SQLUserDAO();
        String login="test";
        int id=-1;
        String permission="teacher";
        boolean expected=false;
        try {
            boolean actual=userDAO.validate(login, id, permission);
            Assert.assertEquals(expected, actual);
        } catch (DAOException e) {
            throw new TestException(e);
        }
    }

}
