package by.bsu.task6.dao.test;

import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.dao.impl.SQLFacultativeDao;
import by.bsu.task6.database.ConnectionPool;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Vlad on 19.12.2015.
 */
public class TestFacultativeDAO {
    @Test
    public void testValidateOnEdit() throws TestException {
        //один вызов на все методы
        ConnectionPool.getInstance().initializate();
        SQLFacultativeDao sqlFacultativeDao = new SQLFacultativeDao();
        int teacherId = 100;
        int facultativeId = 1;
        boolean expected = false;
        try {
            boolean actualInfo = sqlFacultativeDao.validateOnEdit(teacherId, facultativeId);
            Assert.assertEquals(expected, actualInfo);
        } catch (DAOException e) {
            throw new TestException(e);
        }
    }
    @Test
    public void testValidateOnDelete() throws TestException {
        SQLFacultativeDao sqlFacultativeDao = new SQLFacultativeDao();
        int id = 100;
        int teacherId = 1;
        boolean expected = false;
        try {
            boolean actualInfo = sqlFacultativeDao.validateOnDelete(id, teacherId);
            Assert.assertEquals(expected, actualInfo);
        } catch (DAOException e) {
            throw new TestException(e);
        }
    }
    @Test
    public void testValidateOnAdd() throws TestException {
        SQLFacultativeDao sqlFacultativeDao = new SQLFacultativeDao();
        int facultativeId = 1;
        boolean expected = false;
        try {
            boolean actualInfo = sqlFacultativeDao.validateOnAdd(facultativeId);
            Assert.assertEquals(expected, actualInfo);
        } catch (DAOException e) {
            throw new TestException(e);
        }
    }
}
