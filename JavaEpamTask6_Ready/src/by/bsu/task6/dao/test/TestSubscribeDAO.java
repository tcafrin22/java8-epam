package by.bsu.task6.dao.test;

import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.dao.impl.SQLSubscribeDAO;
import by.bsu.task6.database.ConnectionPool;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Vlad on 19.12.2015.
 */
public class TestSubscribeDAO {
    @Test
    public void testValidateOnSubscribe() throws TestException {
        //один вызов на все методы
        ConnectionPool.getInstance().initializate();
        SQLSubscribeDAO sqlSubscribeDAO=new SQLSubscribeDAO();
        int maxStudents=0;
        int facultativeId=1;
        boolean expected=false;
        try {
            boolean actual=sqlSubscribeDAO.validateOnSubscribe(maxStudents,facultativeId);
            Assert.assertEquals(expected,actual);
        } catch (DAOException e) {
            throw new TestException(e);
        }
    }
}
