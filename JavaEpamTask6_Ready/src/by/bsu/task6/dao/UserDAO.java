package by.bsu.task6.dao;

import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.entity.InfoAboutUserInSession;

/**
 * Created by Vlad on 07.11.2015.
 */
/**
 * <p>This interface defines behavior of SQLUserDAO</p>
 *
 * @see by.bsu.task6.dao.impl.SQLUserDAO here the class which implements this interface
 * @author Vlad
 */
public interface UserDAO {
    InfoAboutUserInSession checkUser(String login, String password) throws DAOException;
//    User formUser(String login, String password) throws DAOException;
    void regUser(String firstName,String lastName,int id,String login, String password
            ,String mailOrPhone,String permission)throws DAOException;
    boolean validate(String login,int id,String permission) throws DAOException;
}

