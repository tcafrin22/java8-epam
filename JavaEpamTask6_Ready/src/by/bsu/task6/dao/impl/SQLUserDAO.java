package by.bsu.task6.dao.impl;

import by.bsu.task6.dao.UserDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.database.ConnectionPool;
import by.bsu.task6.database.exception.ConnectionPoolException;
import by.bsu.task6.entity.InfoAboutUserInSession;
import by.bsu.task6.service.util.MD5Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Vlad on 07.11.2015.
 */

/**
 * <p>This class designed for working with Database, especially with table students</p>
 *
 * @author Vlad
 */
public class SQLUserDAO implements UserDAO {
    private static final String PASSWORD_COLUMN = "PASSWORD";
    private static final String STUDENT = "Student";
    private static final String TEACHER = "Teacher";
    private static final String STUDENT_ID = "STUDENT_ID";
    private static final String TEACHER_ID = "TEACHER_ID";
    private static final String FIRST_QUERY = "SELECT * FROM students WHERE LOGIN=?";
    private static final String SECOND_QUERY = "SELECT * FROM teachers WHERE LOGIN=?";

    private static final int MIN_ID = 0;
    private static final int MAX_ID = 9999999;

    private static final String VALIDATE1_QUERY_STUD = "SELECT * FROM students WHERE LOGIN=?";
    private static final String VALIDATE2_QUERY_STUD = "SELECT * FROM students WHERE STUDENT_ID=?";

    private static final String VALIDATE1_QUERY_TEACH = "SELECT * FROM teachers WHERE LOGIN=?";
    private static final String VALIDATE2_QUERY_TEACH = "SELECT * FROM teachers WHERE TEACHER_ID=?";
    private static final String LOGIN = "LOGIN";

    private static final String REG_TEACH_QUERY = "INSERT INTO teachers (TEACHER_ID, FIRSTNAME, LASTNAME, MAIL, LOGIN, PASSWORD) VALUES(?, ?, ?, ?, ?, ?)";
    private static final String REG_STUD_QUERY = "INSERT INTO students (STUDENT_ID, FIRSTNAME, LASTNAME, PHONE, LOGIN, PASSWORD) VALUES(?, ?, ?, ?, ?, ?)";
    private static final Logger logger = LogManager.getLogger(SQLUserDAO.class);

    /**
     * <p>This method check and return user info during log in</p>
     *
     * @return infoAboutUserInSession if there is such user with such login and pass in our system
     * @return null if there is no  user with such login and password in system
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQLException
     */
    @Override
    public InfoAboutUserInSession checkUser(String login, String password) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(FIRST_QUERY);
            statement.setString(1, login);
            rs = statement.executeQuery();

            String enterPassHash = MD5Util.md5Custom(password);

            while (rs.next()) {
                String pass = rs.getString(PASSWORD_COLUMN);
                int id = rs.getInt(STUDENT_ID);
                if (pass.equals(enterPassHash)) {
                    InfoAboutUserInSession infoAboutUserInSession = new InfoAboutUserInSession();
                    infoAboutUserInSession.setFlag(true);
                    infoAboutUserInSession.setStatus(STUDENT);
                    infoAboutUserInSession.setId(id);
                    return infoAboutUserInSession;
                }
            }

            statement = connection.prepareStatement(SECOND_QUERY);
            statement.setString(1, login);
            rs = statement.executeQuery();

            while (rs.next()) {
                String pass = rs.getString(PASSWORD_COLUMN);
                int id = rs.getInt(TEACHER_ID);
                if (pass.equals(enterPassHash)) {
                    InfoAboutUserInSession infoAboutUserInSession = new InfoAboutUserInSession();
                    infoAboutUserInSession.setFlag(true);
                    infoAboutUserInSession.setStatus(TEACHER);
                    infoAboutUserInSession.setId(id);
                    return infoAboutUserInSession;

                }
            }

        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }
        }
        return null;
    }

    /**
     * <p>This method registrate user in system</p>
     *
     * @return true if such login or id not exists and if it's teacher  secret password  for teacher is right
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQLException
     */
    @Override
    public void regUser(String firstName, String lastName, int id, String login,
                        String password, String mailOrPhone, String permission) throws DAOException {

        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        String query;
        if (permission.equals(STUDENT)) {
            query = REG_STUD_QUERY;
        } else {
            query = REG_TEACH_QUERY;
        }
        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(query);

            statement.setInt(1, id);
            statement.setString(2, firstName);
            statement.setString(3, lastName);
            statement.setString(4, mailOrPhone);
            statement.setString(5, login);

            String pass = MD5Util.md5Custom(password);
            statement.setString(6, pass);

            statement.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }
        }
    }

    /**
     * <p>This method validates params of user during registration</p>
     *
     * @return true if such login or id not exists and if it's teacher  secret password  for teacher is right
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQLException
     */

    @Override
    public boolean validate(String login, int id, String permission) throws DAOException {
        if (id <= MIN_ID || id > MAX_ID) {
            return false;
        }
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        if (permission.equals(STUDENT)) {
            try {
                connection = connectorDb.retrieve();
                statement = connection.prepareStatement(VALIDATE1_QUERY_STUD);
                statement.setString(1, login);
                rs = statement.executeQuery();
                while (rs.next()) {
                    if (rs.getString(LOGIN).equals(login)) {
                        return false;
                    }
                }
                statement = connection.prepareStatement(VALIDATE2_QUERY_STUD);
                statement.setInt(1, id);
                rs = statement.executeQuery();
                while (rs.next()) {
                    if (rs.getInt(STUDENT_ID) == id) {
                        return false;
                    }
                }

            } catch (ConnectionPoolException | SQLException e) {
                throw new DAOException(e);
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connectorDb.putback(connection);
                } catch (ConnectionPoolException | SQLException e) {
                    logger.error(e);
                }
            }

        } else {
            try {
                connection = connectorDb.retrieve();
                statement = connection.prepareStatement(VALIDATE1_QUERY_TEACH);
                statement.setString(1, login);
                rs = statement.executeQuery();
                while (rs.next()) {
                    if (rs.getString(LOGIN).equals(login)) {
                        return false;
                    }
                    return false;
                }
                statement = connection.prepareStatement(VALIDATE2_QUERY_TEACH);
                statement.setInt(1, id);
                rs = statement.executeQuery();
                while (rs.next()) {
                    if (rs.getInt(TEACHER_ID) == id) {
                        return false;
                    }
                }

            } catch (ConnectionPoolException | SQLException e) {
                throw new DAOException(e);
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    connectorDb.putback(connection);
                } catch (ConnectionPoolException | SQLException e) {
                    logger.error(e);
                }
            }
        }

        return true;
    }


}
