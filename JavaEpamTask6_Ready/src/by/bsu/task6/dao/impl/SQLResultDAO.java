package by.bsu.task6.dao.impl;

import by.bsu.task6.dao.ResultDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.database.ConnectionPool;
import by.bsu.task6.database.exception.ConnectionPoolException;
import by.bsu.task6.entity.Result;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This class designed for working with Database, especially with table facultatives</p>
 *
 * @author Vlad
 */
public class SQLResultDAO implements ResultDAO {
    private static final String RES = "INSERT INTO result (MARK,COMMENT,STUDENT_ID,FACULTATIVE_ID,FAC_NAME) VALUES(?, ?, ?, ?, ?)";
    private static final String MARK_QUERY = "SELECT MARK,COMMENT,FAC_NAME FROM result WHERE STUDENT_ID=?";
    private static final String MARK_COL = "MARK";
    private static final String FACULTATIVE_NAME_COL = "FAC_NAME";
    private static final String COMMENT_COL = "COMMENT";
    private static final String VALIDATE_ADD = "SELECT FACULTATIVE_ID FROM result WHERE  STUDENT_ID=?";
    private static final String FACULTATIVE_ID_COL = "FACULTATIVE_ID";
    private static final Logger logger = LogManager.getLogger(SQLResultDAO.class);

    /**
     * <p>This method adds result and student into table result </p>
     *
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public void addResult(int facultativeId, int studentId, int mark, String comment, String facultativeName) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(RES);
            statement.setInt(1, mark);
            statement.setString(2, comment);
            statement.setInt(3, studentId);
            statement.setInt(4, facultativeId);
            statement.setString(5, facultativeName);

            statement.executeUpdate();


        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }
        }
    }


    /**
     * <p>This method show result of exact student </p>
     *
     * @return thriples it's mark,comment and facultative name
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */

    @Override
    public List<Result> showResult(int studentId) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        List<Result> results = new ArrayList<>();
        Result result;
        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(MARK_QUERY);
            statement.setInt(1, studentId);
            rs = statement.executeQuery();
            while (rs.next()) {
                result = new Result();

                int mark = rs.getInt(MARK_COL);
                String facultative_NAME = rs.getString(FACULTATIVE_NAME_COL);
                String comment = rs.getString(COMMENT_COL);

                result.setFacultativeName(facultative_NAME);
                result.setMark(mark);
                result.setComment(comment);

                results.add(result);
            }

        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }

        }
        return results;
    }


    /**
     * <p>This method validates if student already have mark on this facultative </p>
     *
     * @return true it's mark,comment and facultative name
     * @return false it's mark,comment and facultative name
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public boolean validateOnAddResult(int facultativeId, int studentId) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(VALIDATE_ADD);

            statement.setInt(1, studentId);
            rs = statement.executeQuery();

            while (rs.next()) {
                if (facultativeId == rs.getInt(FACULTATIVE_ID_COL)) {
                    return false;
                }
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }

        }
        return true;
    }

}
