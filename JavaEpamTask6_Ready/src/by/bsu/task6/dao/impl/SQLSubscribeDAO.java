package by.bsu.task6.dao.impl;

import by.bsu.task6.dao.SubscribeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.database.ConnectionPool;
import by.bsu.task6.database.exception.ConnectionPoolException;
import by.bsu.task6.entity.Student;
import by.bsu.task6.entity.StudentWithMark;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This class designed for working with Database, especially with table student_facultative</p>
 *
 * @author Vlad
 */
public class SQLSubscribeDAO implements SubscribeDAO {
    private static final String SUB = "INSERT INTO student_facultative (FACULTATIVE_ID,STUDENT_ID) VALUES(?, ?)";
    private static final String SHOW_STUD = "SELECT * FROM student_facultative INNER JOIN students ON students.STUDENT_ID=student_facultative.STUDENT_ID WHERE student_facultative.FACULTATIVE_ID=?";
    private static final String VALIDATE_SUB = "SELECT COUNT(STUDENT_ID) FROM student_facultative WHERE FACULTATIVE_ID=?";
    private static final String COUNT_STUDENT_ID = "COUNT(STUDENT_ID)";
    private static final String STUD_COL = "STUDENT_ID";
    private static final String FIRSTNAME_COL = "FIRSTNAME";
    private static final String LASTNAME_COL = "LASTNAME";
    private static final String PHONE_COL = "PHONE";
    private static final Logger logger = LogManager.getLogger(SQLSubscribeDAO.class);

    /**
     * <p>This method subscribes our student on exact facultative </p>
     *
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public void subscribe(int facultativeId, int studentId, int maxStudents) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(SUB);
            statement.setInt(1, facultativeId);
            statement.setInt(2, studentId);
            statement.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }
        }
    }

    /**
     * <p>This method subscribes our student on exact facultative </p>
     *
     * @return students return all students on this facultative id
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    private static final String MARK = "MARK";
    private static final String FIND_MARK_OF_THIS_STUDENT = "Select MARK FROM result Where STUDENT_ID=? AND FACULTATIVE_ID=?";

    @Override
    public List<StudentWithMark> showStud(int facultativeId) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        ResultSet rs1;
        List<Student> students = new ArrayList<>();
        List<StudentWithMark> studentWithMarks = new ArrayList<>();
        Student student;
        StudentWithMark studentWithMark;
        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(SHOW_STUD);
            statement.setInt(1, facultativeId);

            rs = statement.executeQuery();

            while (rs.next()) {
                student = new Student();
                studentWithMark = new StudentWithMark();
                int STUDENT_ID = rs.getInt(STUD_COL);
                String FIRSTNAME = rs.getString(FIRSTNAME_COL);
                String LASTNAME = rs.getString(LASTNAME_COL);
                String PHONE = rs.getString(PHONE_COL);

                student.setFirstname(FIRSTNAME);
                student.setStudentId(STUDENT_ID);
                student.setLastname(LASTNAME);
                student.setPhone(PHONE);

                ////////////
                studentWithMark.setStudent(student);
                statement = connection.prepareStatement(FIND_MARK_OF_THIS_STUDENT);
                statement.setInt(1, STUDENT_ID);
                statement.setInt(2, facultativeId);
                rs1 = statement.executeQuery();
                while (rs1.next()) {
                    int mark = rs1.getInt(MARK);
                    studentWithMark.setMark(mark);
                }
////////////////////////
                students.add(student);

                studentWithMarks.add(studentWithMark);
            }


        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }
        }
//        return students;
        return studentWithMarks;
    }

    /**
     * <p>This checks if there is place for students on this facultative </p>
     *
     * @return false if you can't register on this facultative
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public boolean validateOnSubscribe(int maxStudents, int facultativeId) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(VALIDATE_SUB);
            statement.setInt(1, facultativeId);
            rs = statement.executeQuery();

            while (rs.next()) {
                if (rs.getInt(COUNT_STUDENT_ID) < maxStudents) {
                    return true;
                }
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }
        }
        return false;
    }
}
