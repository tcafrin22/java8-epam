package by.bsu.task6.dao.impl;

import by.bsu.task6.dao.FacultativeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.database.ConnectionPool;
import by.bsu.task6.database.exception.ConnectionPoolException;
import by.bsu.task6.entity.Facultative;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 16.11.2015.
 */

/**
 * <p>This class designed for working with Database, especially with table facultatives</p>
 *
 * @author Vlad
 */
public class SQLFacultativeDao implements FacultativeDAO {
    private static final String FIND_ALL_FAC_QUERY = "SELECT * FROM facultatives";


    private static final String DELETE_FAC_QUERY = "DELETE FROM facultatives WHERE FACULTATIVE_ID=?";
    private static final String DELETE_VALIDATE_QUERY = "SELECT FACULTATIVE_ID FROM facultatives WHERE TEACHER_ID=? ";
    private static final String REG_FAC_QUERY = "INSERT INTO facultatives (FACULTATIVE_ID, MAXSTUDENTS, TEACHER_ID, NAME) VALUES(?, ?, ?, ?)";
    private static final String ADD_VALIDATE_QUERY = "SELECT FACULTATIVE_ID FROM facultatives WHERE FACULTATIVE_ID=?";
    private static final String UPDATE_QUERY = "UPDATE facultatives SET MAXSTUDENTS=?,NAME=? WHERE FACULTATIVE_ID=?";
    private static final String EDIT_VALIDATE_QUERY = "SELECT TEACHER_ID FROM facultatives WHERE FACULTATIVE_ID=?";
    private static final String NAME_COL = "NAME";
    private static final String FIND_SUB_QUERY = "SELECT NAME FROM facultatives INNER JOIN student_facultative ON facultatives.FACULTATIVE_ID=student_facultative.FACULTATIVE_ID WHERE STUDENT_ID=?";

    private static final String FACULTATIVE_ID_COL = "FACULTATIVE_ID";
    private static final String MAXSTUDENTS_COL = "MAXSTUDENTS";
    private static final String TEACHER_ID_COL = "TEACHER_ID";

    private static final Logger logger = LogManager.getLogger(SQLFacultativeDao.class);

    /**
     * <p>This method find all facultatives in table facultatives </p>
     *
     * @return facultatives all facultatives in table
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public List<Facultative> showAllFacultative() throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        List<Facultative> facultatives = new ArrayList<>();
        Facultative facultative;

        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(FIND_ALL_FAC_QUERY);

            rs = statement.executeQuery();
            while (rs.next()) {
                facultative = new Facultative();

                int facultativeId = rs.getInt(FACULTATIVE_ID_COL);
                int maxStudents = rs.getInt(MAXSTUDENTS_COL);
                int teacherId = rs.getInt(TEACHER_ID_COL);
                String name = rs.getString(NAME_COL);

                facultative.setFacultativeId(facultativeId);
                facultative.setMaxStudents(maxStudents);
                facultative.setTeacherId(teacherId);
                facultative.setName(name);

                facultatives.add(facultative);
            }

        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }

        }
        return facultatives;

    }

    /**
     * <p>This method find all facultatives on which i subscribed </p>
     *
     * @return facultatives all facultatives on which i subscribed
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public List<String> showSub(int studId) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        List<String> facultativesnames = new ArrayList<>();
        String name;

        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(FIND_SUB_QUERY);
            statement.setInt(1, studId);
            rs = statement.executeQuery();
            while (rs.next()) {
                name = rs.getString(NAME_COL);
                facultativesnames.add(name);
            }


        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }

        }
        return facultativesnames;
    }

    /**
     * <p>This method edits  facultatives  </p>
     *
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public void editFacultative(int facultativeId, int maxStudents, int teacherId, String name) throws DAOException {

        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(UPDATE_QUERY);
            statement.setInt(1, maxStudents);
            statement.setString(2, name);
            statement.setInt(3, facultativeId);
            statement.executeUpdate();


        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }

        }
    }

    /**
     * <p>This method  validates if i can edit this facultative </p>
     *
     * @return false if it's not mine facultative , so i can't edit it
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public boolean validateOnEdit(int teacherId, int facultativeId) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        try {

            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(EDIT_VALIDATE_QUERY);
            statement.setInt(1, facultativeId);


            rs = statement.executeQuery();


            while (rs.next()) {
                if (teacherId != rs.getInt(TEACHER_ID_COL)) {
                    return false;
                }
            }

        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }

        }

        return true;
    }

    /**
     * <p>This method delete  facultative  </p>
     *
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public void deleteFacultative(int id) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;


        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(DELETE_FAC_QUERY);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }
        }
    }

    /**
     * <p>This method  validates if i can delete this facultative </p>
     *
     * @return false if it's not mine facultative , so i can't delete it
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public boolean validateOnDelete(int id, int teacherId) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        try {
            connection = connectorDb.retrieve();
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(DELETE_VALIDATE_QUERY);
            statement.setInt(1, teacherId);

            rs = statement.executeQuery();

            while (rs.next()) {
                if (id == rs.getInt(FACULTATIVE_ID_COL)) {
                    return true;
                }
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }

        }

        return false;
    }

    /**
     * <p>This method adds  facultative  </p>
     *
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public void addFacultative(int facultativeId, int teacherId, int maxStudents, String name) throws DAOException {
        ConnectionPool connectorDb = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(REG_FAC_QUERY);
            statement.setInt(1, facultativeId);
            statement.setInt(2, maxStudents);
            statement.setInt(3, teacherId);
            statement.setString(4, name);
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }
        }
    }

    /**
     * <p>This method  validates if i can add this facultative(Maybe it's already exists) </p>
     *
     * @return false if there are  facultatives with such id
     * @throws by.bsu.task6.dao.exception.DAOException if he catches Any SQL Exception
     */
    @Override
    public boolean validateOnAdd(int facultativeId) throws DAOException {

        ConnectionPool connectorDb = ConnectionPool.getInstance();

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        try {
            connection = connectorDb.retrieve();
            statement = connection.prepareStatement(ADD_VALIDATE_QUERY);
            statement.setInt(1, facultativeId);


            rs = statement.executeQuery();


            while (rs.next()) {
                if (facultativeId == rs.getInt(FACULTATIVE_ID_COL)) {
                    return false;
                }
            }

        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                connectorDb.putback(connection);
            } catch (ConnectionPoolException | SQLException e) {
                logger.error(e);
            }
        }
        return true;
    }


}
