package by.bsu.task6.database.exception;

/**
 * Created by Vlad on 15.11.2015.
 */

/**
 * <p>This class designed for  my own Exception- ConnectionPoolException;It can be thrown in Connection Pool;</p>
 *
 * @author Vlad
 */
public class ConnectionPoolException extends Exception {
    public ConnectionPoolException() {
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(Throwable exception) {
        super(exception);
    }

    public ConnectionPoolException(String message, Throwable exception) {
        super(message, exception);
    }
}
