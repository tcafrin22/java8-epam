package by.bsu.task6.entity;

/**
 * Created by Vlad on 09.11.2015.
 */

import java.io.Serializable;

/**
 * <p>This class describes such essence as  InfoAboutUserInSession; This is special object
 * which contains flag,status and id name  and it will be shown on main.jsp </p>
 *
 * @author Vlad
 */
public class InfoAboutUserInSession implements Serializable {
    private boolean flag;
    private String status;
    private int id;

    public InfoAboutUserInSession() {
    }

    public InfoAboutUserInSession(boolean flag, String status, int id) {
        this.flag = flag;
        this.status = status;
        this.id = id;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }

        InfoAboutUserInSession that = (InfoAboutUserInSession) o;

        if (flag != that.flag) return false;
        if (id != that.id) return false;
        if (!status.equals(that.status)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (flag ? 1 : 0);
        result = 31 * result + status.hashCode();
        result = 31 * result + id;
        return result;
    }
}
