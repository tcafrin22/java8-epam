package by.bsu.task6.entity;

/**
 * Created by Vlad on 17.11.2015.
 */

import java.io.Serializable;

/**
 * <p>This class describes such essence as  Facultative</p>
 *
 * @author Vlad
 */
public class Facultative implements Serializable {
    private int facultativeId;
    private int maxStudents;
    private int teacherId;
    private String name;


    public int getFacultativeId() {
        return facultativeId;
    }

    public void setFacultativeId(int facultativeId) {
        this.facultativeId = facultativeId;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }

        Facultative that = (Facultative) o;

        if (facultativeId != that.facultativeId) return false;
        if (maxStudents != that.maxStudents) return false;
        if (teacherId != that.teacherId) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = facultativeId;
        result = 31 * result + maxStudents;
        result = 31 * result + teacherId;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Facultative{" +
                "facultativeId=" + facultativeId +
                ", maxStudents=" + maxStudents +
                ", teacherId=" + teacherId +
                ", name='" + name + '\'' +
                '}';
    }
}
