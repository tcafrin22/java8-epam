package by.bsu.task6.entity;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by Vlad on 21.11.2015.
 */

/**
 * <p>This class designed my own tag</p>
 *
 * @author Vlad
 */
public class CopyrightTag extends TagSupport {
    private static final String footer = "Copyright by Vlad Zhukovsky 2015 ";
    private static final String footer_tag_start = "<footer>";
    private static final String footer_tag_end = "</footer>";

    @Override
    public int doStartTag() throws JspTagException {
        try {
            JspWriter out = pageContext.getOut();
            out.write(footer_tag_start);
            out.write(footer);

        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return EVAL_BODY_INCLUDE;
    }


    @Override
    public int doEndTag() throws JspTagException {
        try {
            pageContext.getOut().write(footer_tag_end);
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return EVAL_PAGE;
    }
}


