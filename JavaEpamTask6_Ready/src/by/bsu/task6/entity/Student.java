package by.bsu.task6.entity;

/**
 * Created by Vlad on 03.11.2015.
 */

import java.io.Serializable;

/**
 * <p>This class describes such essence as  Student</p>
 *
 * @author Vlad
 */
public class Student extends User implements Serializable {
    private int studentId;
    private String firstname;
    private String lastname;
    private String phone;

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }

        Student student = (Student) o;

        if (studentId != student.studentId) return false;
        if (!firstname.equals(student.firstname)) return false;
        if (!lastname.equals(student.lastname)) return false;
        if (!phone.equals(student.phone)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = studentId;
        result = 31 * result + firstname.hashCode();
        result = 31 * result + lastname.hashCode();
        result = 31 * result + phone.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
