package by.bsu.task6.entity;

import java.io.Serializable;

/**
 * Created by Vlad on 05.12.2015.
 */
public class StudentWithMark implements Serializable {
    private Student student;
    private int mark;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }

        StudentWithMark that = (StudentWithMark) o;

        if (mark != that.mark) return false;
        if (!student.equals(that.student)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = student.hashCode();
        result = 31 * result + mark;
        return result;
    }
}
