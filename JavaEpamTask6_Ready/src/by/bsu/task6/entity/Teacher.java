package by.bsu.task6.entity;

/**
 * Created by Vlad on 03.11.2015.
 */

import java.io.Serializable;

/**
 * <p>This class describes such essence as  Teacher</p>
 *
 * @author Vlad
 */
public class Teacher extends User implements Serializable {
    private int teacherId;
    private String firstname;
    private String lastname;
    private String mail;


    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }

        Teacher teacher = (Teacher) o;

        if (teacherId != teacher.teacherId) return false;
        if (!firstname.equals(teacher.firstname)) return false;
        if (!lastname.equals(teacher.lastname)) return false;
        if (!mail.equals(teacher.mail)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = teacherId;
        result = 31 * result + firstname.hashCode();
        result = 31 * result + lastname.hashCode();
        result = 31 * result + mail.hashCode();
        return result;
    }
}
