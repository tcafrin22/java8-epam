package by.bsu.task6.entity;

/**
 * Created by Vlad on 20.11.2015.
 */

import java.io.Serializable;

/**
 * <p>This class describes such essence as  Result; This is special object
 * which contains mark,comment and facultative name and it will be shown on main.jsp </p>
 *
 * @author Vlad
 */
public class Result implements Serializable {
    private int mark;
    private String comment;
    private String facultativeName;

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFacultativeName() {
        return facultativeName;
    }

    public void setFacultativeName(String facultativeName) {
        this.facultativeName = facultativeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }

        Result result = (Result) o;

        if (mark != result.mark) return false;
        if (comment != null ? !comment.equals(result.comment) : result.comment != null) return false;
        if (!facultativeName.equals(result.facultativeName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = mark;
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + facultativeName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "TResult" +
                "mark=" + mark +
                ", CCOMMENT='" + comment + '\'' +
                ", facultativeName='" + facultativeName + '\'' +
                '}';
    }

}
