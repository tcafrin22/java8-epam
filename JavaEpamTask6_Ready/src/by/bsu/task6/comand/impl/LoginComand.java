package by.bsu.task6.comand.impl;

import by.bsu.task6.comand.ICommand;
import by.bsu.task6.comand.exception.ComandException;
import by.bsu.task6.resource.ConfigurationManager;
import by.bsu.task6.resource.MessageManager;
import by.bsu.task6.service.impl.LoginService;
import by.bsu.task6.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 04.11.2015.
 */

/**
 * <p>This class designed for creating LoginComand</p>
 *
 * @author Vlad
 */
public class LoginComand implements ICommand {
    private static final String PAGE_MAIN = "path.page.main";
    private static final String USER = "user";
    private static final String ERROR_LOGIN_MESSAGE = "errorLoginPassMessage";
    private static final String MESSAGE_LOGIN_ERROR = "message.loginerror";
    private static final String PAGE_LOGIN = "path.page.login";
    private static final Logger logger = LogManager.getLogger(LoginComand.class);

    /**
     * <p>This method calls the appropriate service - LoginService;If we can't get USER from session or we cathes
     * ServiceException we set an attribute errorLoginPassMessage for our request</p>
     *
     * @param request A request that comes to this command
     * @return page returns the address of the page you want to go
     * @throws by.bsu.task6.comand.exception.ComandException if he catches ServiceException
     */
    @Override
    public String execute(HttpServletRequest request) throws ComandException {
        String page;
        try {
            LoginService.getInstance().doService(request);
        } catch (ServiceException e) {
            logger.error(e);
            request.setAttribute(ERROR_LOGIN_MESSAGE,
                    MessageManager.getProperty(MESSAGE_LOGIN_ERROR));
            return page = ConfigurationManager.getProperty(PAGE_LOGIN);
        }
        page = ConfigurationManager.getProperty(PAGE_MAIN);

        if (request.getSession().getAttribute(USER) != null) {
            return page;
        } else {
            request.setAttribute(ERROR_LOGIN_MESSAGE,
                    MessageManager.getProperty(MESSAGE_LOGIN_ERROR));
            page = ConfigurationManager.getProperty(PAGE_LOGIN);
        }
        return page;
    }

}
