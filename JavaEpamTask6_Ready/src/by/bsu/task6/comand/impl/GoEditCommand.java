package by.bsu.task6.comand.impl;

import by.bsu.task6.comand.ICommand;
import by.bsu.task6.comand.exception.ComandException;
import by.bsu.task6.resource.ConfigurationManager;
import by.bsu.task6.service.exception.ServiceException;
import by.bsu.task6.service.impl.GoEditService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This class designed for creating GoEditCommand command</p>
 *
 * @author Vlad
 */
public class GoEditCommand implements ICommand {
    private static final String PAGE_ADD = "path.page.edit";
    private static final String ID = "id";
    private static final String MAXSTUDENTS = "maxstudents";
    private static final String NAME = "name";

    /**
     * <p>This method calls the appropriate service - GoEditService</p>
     *
     * @param request A request that comes to this command
     * @return page returns the address of the page you want to go
     * @throws by.bsu.task6.comand.exception.ComandException if he catches ServiceException
     */
    @Override
    public String execute(HttpServletRequest request) throws ComandException {
        String page;
        try {
            GoEditService.getInstance().doService(request);
        } catch (ServiceException e) {
            throw new ComandException(e);
        }
        request.setAttribute(ID, request.getParameter(ID));
        request.setAttribute(MAXSTUDENTS, request.getParameter(MAXSTUDENTS));
        request.setAttribute(NAME, request.getParameter(NAME));
        page = ConfigurationManager.getProperty(PAGE_ADD);
        return page;
    }
}
