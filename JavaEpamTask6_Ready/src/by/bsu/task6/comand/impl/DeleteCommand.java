package by.bsu.task6.comand.impl;

import by.bsu.task6.comand.ICommand;
import by.bsu.task6.comand.exception.ComandException;
import by.bsu.task6.service.exception.ServiceException;
import by.bsu.task6.service.impl.DeleteService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 18.11.2015.
 */

/**
 * <p>This class designed for deleting facultative command</p>
 *
 * @author Vlad
 */
public class DeleteCommand implements ICommand {
    private static final String PAGE = "page";

    /**
     * <p>This method calls the appropriate service - DeleteService</p>
     *
     * @param request A request that comes to this command
     * @return page returns the address of the page you want to go
     * @throws by.bsu.task6.comand.exception.ComandException if he catches ServiceException
     */
    @Override
    public String execute(HttpServletRequest request) throws ComandException {
        try {
            DeleteService.getInstance().doService(request);
        } catch (ServiceException e) {
            throw new ComandException(e);
        }
        String page = request.getParameter(PAGE);
        return page;
    }
}
