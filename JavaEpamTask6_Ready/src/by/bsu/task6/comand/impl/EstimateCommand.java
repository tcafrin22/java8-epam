package by.bsu.task6.comand.impl;

import by.bsu.task6.comand.ICommand;
import by.bsu.task6.comand.exception.ComandException;
import by.bsu.task6.resource.ConfigurationManager;
import by.bsu.task6.service.exception.ServiceException;
import by.bsu.task6.service.impl.EstimateService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This class designed for creating EstimateCommand </p>
 *
 * @author Vlad
 */
public class EstimateCommand implements ICommand {
    private static final String MAIN_PAGE = "path.page.main";

    /**
     * <p>This method calls the appropriate service - EstimateService</p>
     *
     * @param request A request that comes to this command
     * @return page returns the address of the page you want to go
     * @throws by.bsu.task6.comand.exception.ComandException if he catches ServiceException
     */
    @Override
    public String execute(HttpServletRequest request) throws ComandException {
        String page;
        try {
            EstimateService.getInstance().doService(request);
        } catch (ServiceException e) {
            throw new ComandException(e);
        }
        page = ConfigurationManager.getProperty(MAIN_PAGE);
        return page;
    }
}
