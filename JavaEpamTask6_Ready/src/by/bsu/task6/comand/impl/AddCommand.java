package by.bsu.task6.comand.impl;

import by.bsu.task6.comand.ICommand;
import by.bsu.task6.comand.exception.ComandException;
import by.bsu.task6.resource.ConfigurationManager;
import by.bsu.task6.resource.MessageManager;
import by.bsu.task6.service.exception.ServiceException;
import by.bsu.task6.service.impl.AddService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 18.11.2015.
 */

/**
 * <p>This class designed for addition facultative command</p>
 *
 * @author Vlad
 */

public class AddCommand implements ICommand {
    private static final String PAGE_ADD = "path.page.addfac";
    private static final String ERROR_LOGIN_MESSAGE = "errorCreateFac";
    private static final String MESSAGE_LOGIN_ERROR = "message.erorcreatefac";
    private static final Logger logger = LogManager.getLogger(AddCommand.class);

    /**
     * <p>This method calls the appropriate service - AddService</p>
     *
     * @param request A request that comes to this command
     * @return page returns the address of the page you want to go
     * @throws by.bsu.task6.comand.exception.ComandException if he catches ServiceException
     */
    @Override
    public String execute(HttpServletRequest request) throws ComandException {
        String page;
        try {
            AddService.getInstance().doService(request);
        } catch (ServiceException e) {
            logger.error(e);
            request.setAttribute(ERROR_LOGIN_MESSAGE,
                    MessageManager.getProperty(MESSAGE_LOGIN_ERROR));
        }
        page = ConfigurationManager.getProperty(PAGE_ADD);
        return page;
    }
}
