package by.bsu.task6.comand.impl;

import by.bsu.task6.comand.ICommand;
import by.bsu.task6.comand.exception.ComandException;
import by.bsu.task6.resource.ConfigurationManager;
import by.bsu.task6.resource.MessageManager;
import by.bsu.task6.service.exception.ServiceException;
import by.bsu.task6.service.impl.GoEstimateService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This class designed for  creating GoEstimateCommand</p>
 *
 * @author Vlad
 */

public class GoEstimateCommand implements ICommand {
    private static final String ESTIMATE_PAGE = "path.page.estimate";
    private static final String MAIN_PAGE = "path.page.main";

    private static final String STUDENT_ID = "studid";
    private static final String FACULTATIVE_ID = "facid";
    private static final String FACULTATIVE_NAME = "facname";

    private static final String ERROR_EST_MESSAGE = "errorEstimate";
    private static final String MESSAGE_EST_ERROR = "message.erorcestimate";
    private static final Logger logger = LogManager.getLogger(GoEstimateCommand.class);

    /**
     * <p>This method calls the appropriate service - GoEstimateService;Also sets attribute STUDENT_ID,FACULTATIVE_ID,
     * FACULTATIVE_NAME to request</p>
     *
     * @param request A request that comes to this command
     * @return page returns the address of the page you want to go
     * @throws by.bsu.task6.comand.exception.ComandException if he catches ServiceException
     */
    @Override
    public String execute(HttpServletRequest request) throws ComandException {
        String page;
        try {
            GoEstimateService.getInstance().doService(request);
        } catch (ServiceException e) {
            logger.error(e);
            request.setAttribute(ERROR_EST_MESSAGE,
                    MessageManager.getProperty(MESSAGE_EST_ERROR));
            return page = ConfigurationManager.getProperty(MAIN_PAGE);
        }

        request.setAttribute(STUDENT_ID, request.getParameter(STUDENT_ID));
        request.setAttribute(FACULTATIVE_ID, request.getParameter(FACULTATIVE_ID));
        request.setAttribute(FACULTATIVE_NAME, request.getParameter(FACULTATIVE_NAME));

        page = ConfigurationManager.getProperty(ESTIMATE_PAGE);
        return page;
    }
}
