package by.bsu.task6.comand.impl;

import by.bsu.task6.comand.ICommand;
import by.bsu.task6.comand.exception.ComandException;
import by.bsu.task6.resource.ConfigurationManager;
import by.bsu.task6.resource.MessageManager;
import by.bsu.task6.service.impl.RegistrationService;
import by.bsu.task6.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 09.11.2015.
 */

/**
 * <p>This class designed for creating RegistrationComand</p>
 *
 * @author Vlad
 */
public class RegistrationComand implements ICommand {
    private static final String PAGE_LOGIN = "path.page.login";
    private static final String PAGE_REG = "path.page.reg";
    private static final String ERROR_MAIL_MESSAGE = "wrongMail";
    private static final String ERROR_PHONE_MESSAGE = "wrongPhone";
    private static final String ERROR_REG_MESSAGE = "errorRegPassMessage";
    private static final String MESSAGE_REG_ERROR = "message.regerror";

    private static final Logger logger = LogManager.getLogger(RegistrationComand.class);


    /**
     * <p>This method calls the appropriate service - RegistrationService;if our method catches ServiceException
     * we would set attribute errorRegPassMessage  for our request </p>
     *
     * @param request A request that comes to this command
     * @return page returns the address of the page you want to go
     * @throws by.bsu.task6.comand.exception.ComandException if he catches ServiceException
     */
    @Override
    public String execute(HttpServletRequest request) throws ComandException {
        String page;
        try {
            RegistrationService.getInstance().doService(request);
        } catch (ServiceException e) {
            logger.error(e);
            if (request.getAttribute(ERROR_MAIL_MESSAGE) == null &&
                    request.getAttribute(ERROR_PHONE_MESSAGE) == null) {
                request.setAttribute(ERROR_REG_MESSAGE,
                        MessageManager.getProperty(MESSAGE_REG_ERROR));
            }
            page = ConfigurationManager.getProperty(PAGE_REG);
            return page;
        }

        page = ConfigurationManager.getProperty(PAGE_LOGIN);
        return page;
    }
}
