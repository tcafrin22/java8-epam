package by.bsu.task6.comand;

import by.bsu.task6.comand.exception.ComandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 04.11.2015.
 */

/**
 * <p>This interface should be implemented by every command </p>
 *
 * @author Vlad
 */
public interface ICommand {
    String execute(HttpServletRequest request) throws ComandException;
}
