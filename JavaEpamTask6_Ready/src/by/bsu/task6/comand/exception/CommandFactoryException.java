package by.bsu.task6.comand.exception;

/**
 * Created by Vlad on 28.11.2015.
 */
public class CommandFactoryException extends Exception {
    public CommandFactoryException(){
    }
    public CommandFactoryException(String message){
        super(message);
    }
    public CommandFactoryException(Throwable exception){
        super(exception);
    }
    public CommandFactoryException(String message, Throwable exception) {
        super(message, exception);
    }
}
