package by.bsu.task6.comand.exception;

/**
 * Created by Vlad on 07.11.2015.
 */

/**
 * <p>This class designed for  my own Exception- ComandException;It can be thrown in any command;</p>
 *
 * @author Vlad
 */
public class ComandException extends Exception {
    public ComandException() {
    }

    public ComandException(String message) {
        super(message);
    }

    public ComandException(Throwable exception) {
        super(exception);
    }

    public ComandException(String message, Throwable exception) {
        super(message, exception);
    }
}
