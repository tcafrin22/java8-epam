package by.bsu.task6.comand;

import by.bsu.task6.comand.impl.*;

/**
 * Created by Vlad on 04.11.2015.
 */
/**
 * <p>Enum VariantCommand with all variant's of command  </p>
 *
 * @author Vlad
 */

/**
 * <p>In this enum we declares variants of commands</p>
 *
 * @author Vlad
 */
public enum VariantCommand {

    LOGIN {
        {
            this.command = new LoginComand();
        }
    },
    LOCALE {
        {
            this.command = new LocaleCommand();
        }
    },
    SHOWFACULTATIVE {
        {
            this.command = new ShowFacultativeCommand();
        }
    },
    REGISTRATION {
        {
            this.command = new RegistrationComand();
        }
    },
    SHOWMARK {
        {
            this.command = new ShowMarkCommand();
        }
    },
    SHOWSUB {
        {
            this.command = new ShowSubscriberCommand();
        }
    },
    GOESTIMATE {
        {
            this.command = new GoEstimateCommand();
        }
    },
    ESTIMATE {
        {
            this.command = new EstimateCommand();
        }
    },
    SHOWSTUDENTS {
        {
            this.command = new ShowStudComand();
        }
    },

    SUBSCRIBE {
        {
            this.command = new SubscribeCommand();
        }
    },


    ADDFAC {
        {
            this.command = new AddCommand();
        }
    },
    EDIT {
        {
            this.command = new EditCommand();
        }
    },
    GOEDIT {
        {
            this.command = new GoEditCommand();
        }
    },
    DELETE {
        {
            this.command = new DeleteCommand();
        }
    },

    LOGOUT {
        {
            this.command = new LogoutComand();
        }
    };

    ICommand command;

    /**
     * <p>This method returns command initialized above</p>
     *
     * @return command   exact command
     */
    public ICommand getCurrentCommand() {
        return command;
    }

}
