package by.bsu.task6.comand;

import by.bsu.task6.comand.exception.CommandFactoryException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Vlad on 04.11.2015.
 */

/**
 * <p>This class designed for  creation command</p>
 *
 * @author Vlad
 */

public final class CommandFactory {


    private static final Logger logger = LogManager.getLogger(CommandFactory.class);
    private static final CommandFactory instance = new CommandFactory();
    public static CommandFactory getInstance() {
        return instance;
    }
    /**
     * <p>This method defines the command</p>
     *
     * @param action An action that comes to be defined
     * @return current returns the exact command
     */

    public ICommand defineCommand(final String action) throws CommandFactoryException {
        ICommand current;
        if (action == null || action.isEmpty()) {
            throw new CommandFactoryException("Wrong argument");
        }
        try {
            VariantCommand currentEnum =
                    VariantCommand.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            logger.error(e);
            throw new CommandFactoryException(e);
        }
        return current;
    }
}
