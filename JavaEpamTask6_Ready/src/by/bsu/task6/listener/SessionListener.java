package by.bsu.task6.listener;

import by.bsu.task6.database.ConnectionPool;
import by.bsu.task6.database.exception.ConnectionPoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by Vlad on 10.11.2015.
 */

/**
 * <p>This class is our web-app Listener </p>
 *
 * @author Vlad
 */
public class SessionListener implements ServletContextListener {
    private static final Logger logger = LogManager.getLogger(SessionListener.class);

    /**
     * <p>This method initializes our Connection Pool</p>
     *
     * @param servletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ConnectionPool.getInstance().initializate();
    }

    /**
     * <p>This method destroys our Connection Pool</p>
     *
     * @param servletContextEvent
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            ConnectionPool.getInstance().destroy();
        } catch (ConnectionPoolException e) {
            logger.error(e);
        }
    }
}
