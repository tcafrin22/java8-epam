package by.bsu.task6.resource;

import java.util.ResourceBundle;

/**
 * Created by Vlad on 04.11.2015.
 */

/**
 * <p>This class was created for getting path to page from package resources file config</p>
 *
 * @author Vlad
 */
public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");

    // класс извлекает информацию из файла config.properties
    private ConfigurationManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
