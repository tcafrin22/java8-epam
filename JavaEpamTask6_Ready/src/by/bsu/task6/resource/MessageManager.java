package by.bsu.task6.resource;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Vlad on 04.11.2015.
 */

/**
 * <p>This class was created for getting getting messages from package resources file resource bundle message
 * and this bundle using for localization</p>
 *
 * @author Vlad
 */
public class MessageManager {
    private static final Locale EN = new Locale("en", "EN");
    private static final Locale RU = new Locale("ru", "RU");

    private static ResourceBundle resourceBundle =
            ResourceBundle.getBundle("resources.message", RU);

    // класс извлекает информацию из файла messages_ru.properties
    public MessageManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }

    public static void setResourceBundle(ResourceBundle resourceBundle) {
        MessageManager.resourceBundle = resourceBundle;
    }

    public static ResourceBundle getResourceBundle() {
        return resourceBundle;
    }
}
