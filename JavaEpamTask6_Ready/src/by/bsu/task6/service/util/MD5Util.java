package by.bsu.task6.service.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Vlad on 16.11.2015.
 */

/**
 * <p>This made to encode our password </p>
 *
 * @author Vlad
 */
public class MD5Util {
    private static final String MD5 = "MD5";
    private static final String ZERO = "0";

    /**
     * <p>This method enncode our password according to MD5</p>
     *
     * @param st our password before encode
     * @return md5Hex our password after encode
     */
    public static String md5Custom(String st) {
        MessageDigest messageDigest;
        byte[] digest = new byte[0];

        try {
            messageDigest = MessageDigest.getInstance(MD5);
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        }

        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);

        while (md5Hex.length() < 32) {
            md5Hex = ZERO + md5Hex;
        }

        return md5Hex;
    }
}
