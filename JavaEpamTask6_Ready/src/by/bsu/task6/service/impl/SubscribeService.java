package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.SubscribeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.resource.MessageManager;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This class designed for processing  subscribe command</p>
 *
 * @author Vlad
 */
public class SubscribeService implements IService {

    private static final SubscribeService instance = new SubscribeService();

    public static SubscribeService getInstance() {
        return instance;
    }

    private static final String ID = "id";
    private static final String MAX_STUDENTS = "maxstudents";
    private static final String ERROR_NO_SPACE = "noSpace";
    private static final String MESSAGE_ID_ERROR = "message.nospace";

    /**
     * <p>This method calls the appropriate SQLSubscribeDAOSubDAO and it's method subscribe</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        SubscribeDAO subscribeDAO = daoFactory.getSubDAO();

        try {
            int facultativeId = Integer.parseInt(request.getParameter(ID));
            int studentId = Integer.parseInt(request.getSession().getAttribute(ID).toString());
            int maxStudents = Integer.parseInt(request.getParameter(MAX_STUDENTS));
            if (!subscribeDAO.validateOnSubscribe(maxStudents, facultativeId)) {
                //проверяю есть ли там еще места
                request.setAttribute(ERROR_NO_SPACE,
                        MessageManager.getProperty(MESSAGE_ID_ERROR));
                throw new ServiceException("Exception in subscribe validation");
            }
            subscribeDAO.subscribe(facultativeId, studentId, maxStudents);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


}
