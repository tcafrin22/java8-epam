package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.UserDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.entity.InfoAboutUserInSession;
import by.bsu.task6.entity.User;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 07.11.2015.
 */

/**
 * <p>This class designed for processing  log in  command</p>
 *
 * @author Vlad
 */
public class LoginService implements IService {
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String USER = "user";
    private static final String STATUS = "status";
    private static final String ID = "id";

    private static final LoginService instance = new LoginService();

    public static LoginService getInstance() {
        return instance;
    }


    private static final String EXCEPTION_MESSAGE = "Some fields are empty empty";

    /**
     * <p>This method calls the appropriate DAO - SQLUserDAO and it's method checkUser
     * If u logged also sets some attributes such as id,login and status into session</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);

        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        UserDAO userDAO = daoFactory.getUserDAO();
        InfoAboutUserInSession status;

        if (validate(login, password)) {
            try {
                status = userDAO.checkUser(login, password);
            } catch (DAOException e) {
                throw new ServiceException(e);
            }

        } else {
            throw new ServiceException(EXCEPTION_MESSAGE);
        }

        if (status != null) {
            User user;
            user = new User(login, password);
            request.getSession(true).setAttribute(USER, user.getLogin());
            request.getSession(true).setAttribute(STATUS, status.getStatus());
            request.getSession(true).setAttribute(ID, status.getId());
        }

    }

    /**
     * <p>validates if the password or login are empty</p>
     *
     * @param login    entered login
     * @param password entered password
     */
    private final boolean validate(String login, String password) {
        if (login.isEmpty() || password.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}
