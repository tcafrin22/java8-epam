package by.bsu.task6.service.impl;

import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 09.11.2015.
 */

/**
 * <p>This class designed for processing  log out  command</p>
 *
 * @author Vlad
 */
public class LogoutService implements IService {

    private static final LogoutService instance = new LogoutService();

    public static LogoutService getInstance() {
        return instance;
    }

    /**
     * <p>This method invalidates our session and makes log out </p>
     *
     * @param request A request that comes to this service
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        request.getSession().invalidate();
    }
}
