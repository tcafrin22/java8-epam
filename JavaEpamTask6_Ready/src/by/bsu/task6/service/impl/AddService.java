package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.FacultativeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 18.11.2015.
 */

/**
 * <p>This class designed for processing add facultative command</p>
 *
 * @author Vlad
 */
public class AddService implements IService {
    private static final String FACULTATIVE_ID = "facid";
    private static final String TEACHER_ID = "id";
    private static final String MAXSTUDENTS = "maxstudents";
    private static final String FACULTATIVENAME = "facname";

    private static final AddService instance = new AddService();

    public static AddService getInstance() {
        return instance;
    }

    private static final int MIN_MAXSTUDENTS = 5;
    private static final int MAX_MAXSTUDENTS = 30;
    private static final int FACULTATIVE_ID_MIN = 1;
    private static final int FACULTATIVE_ID_MAX = 9999999;
    private static final String STATUS = "status";
    /**
     * <p>This method calls the appropriate DAO - SQLFacultativeDAO and it's method addFacultative</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {


        if(request.getSession().getAttribute(STATUS) == null){
            throw new ServiceException("You are already logouted, please log in");
        }

        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        FacultativeDAO facultativeDAO = daoFactory.getFacultativeDAO();

        try {
            int facultativeId = Integer.parseInt(request.getParameter(FACULTATIVE_ID));
            int teacherId = Integer.parseInt(request.getSession().getAttribute(TEACHER_ID).toString());
            int maxStudents = Integer.parseInt(request.getParameter(MAXSTUDENTS));
            if (!validate(request, maxStudents, facultativeId)) {
                throw new ServiceException("Some fields are empty,or very big(small) value of maxStudents/facultative id");
            }
            String name = request.getParameter(FACULTATIVENAME);
            if (!facultativeDAO.validateOnAdd(facultativeId)) {
                throw new ServiceException("Facultative with such id already exists");
            }

            facultativeDAO.addFacultative(facultativeId, teacherId, maxStudents, name);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    /**
     * <p>This method validates such input fields as max student and facultative id;Validates only value </p>
     *
     * @param request     A request that comes to this method
     * @param maxStudents number of maximum students in this facultative
     * @param facId      A request that comes to this method
     */

    private final boolean validate(HttpServletRequest request, int maxStudents, int facId) {
        if (request.getParameter(FACULTATIVE_ID).isEmpty() || request.getParameter(MAXSTUDENTS).isEmpty()
                || request.getParameter(FACULTATIVENAME).isEmpty()) {
            return false;
        }
        if (maxStudents < MIN_MAXSTUDENTS || maxStudents > MAX_MAXSTUDENTS) {
            return false;
        }
        if (facId < FACULTATIVE_ID_MIN || facId > FACULTATIVE_ID_MAX) {
            return false;
        }
        return true;
    }
}
