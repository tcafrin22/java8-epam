package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.UserDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.resource.MessageManager;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vlad on 09.11.2015.
 */

/**
 * <p>This class designed for processing  registration  command</p>
 *
 * @author Vlad
 */
public class RegistrationService implements IService {
    private static final String ERROR_MAIL_MESSAGE = "wrongMail";
    private static final String MESSAGE_MAIL_ERROR = "message.wrongmail";
    private static final RegistrationService instance = new RegistrationService();
    private static String VALIDATE_MAIL = "^[0-9a-z]+[-\\._0-9a-z]*@[0-9a-z]+[-\\._^0-9a-z]*[0-9a-z]+[\\.]{1}[a-z]{2,6}$";
    private static String VALIDATE_PHONE = "[1-9]{1}[\\d]{2}-{1}[\\d]{2}-{1}[\\d]+";

    public static RegistrationService getInstance() {
        return instance;
    }

    private static final String ERROR_PHONE_MESSAGE = "wrongPhone";
    private static final String MESSAGE_PHONE_ERROR = "message.wrongphone";
    private static final String SECRET_PASSWORD_FOR_TEACHER = "qwerty";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final String ID = "id";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String MAIL = "mail";
    private static final String TELPHONE = "telephone";
    private static final String PASSWORD_FOR_TEACHER = "passwordForTeachers";

    /**
     * <p>This method calls the appropriate DAO - SQLUserDAO and it's method regUser</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */


    @Override
    public void doService(HttpServletRequest request) throws ServiceException {

        String firstname = request.getParameter(FIRSTNAME);

        String lastname = request.getParameter(LASTNAME);

        int id = Integer.parseInt(request.getParameter(ID));

        String login = request.getParameter(LOGIN);

        String password = request.getParameter(PASSWORD);

        String mail = request.getParameter(MAIL);

        String telephone = request.getParameter(TELPHONE);

        String passwordForTeachers = request.getParameter(PASSWORD_FOR_TEACHER);


        if (!mail.isEmpty()) {
            if (!validateMail(mail)) {
                request.setAttribute(ERROR_MAIL_MESSAGE,
                        MessageManager.getProperty(MESSAGE_MAIL_ERROR));
                throw new ServiceException("Invalid mail");
            }
        }

        if (!telephone.isEmpty()) {
            if (!validatePhone(telephone)) {
                request.setAttribute(ERROR_PHONE_MESSAGE,
                        MessageManager.getProperty(MESSAGE_PHONE_ERROR));
                throw new ServiceException("Invalid phone");
            }

        }

        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        UserDAO userDAO = daoFactory.getUserDAO();
        try {
            if (passwordForTeachers.isEmpty()) {
                String permission = "Student";
                if (!userDAO.validate(login, id, permission)) {

                    throw new ServiceException("Exception in reg validation");
                }
                userDAO.regUser(firstname, lastname, id, login, password, telephone, permission);
            } else {
                if (passwordForTeachers.equals(SECRET_PASSWORD_FOR_TEACHER)) {
                    String permission = "Teacher";
                    if (!userDAO.validate(login, id, permission)) {
                        throw new ServiceException("Exception in reg validation");
                    }
                    userDAO.regUser(firstname, lastname, id, login, password, mail, permission);
                } else {
                    throw new ServiceException("No permission to reg teacher");
                }
            }
        } catch (DAOException | NumberFormatException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * <p>This method validates the email</p>
     *
     * @return false if  it's wrong format of email
     */
//            1.имя не может начинаться с "-", ".", "_"
//            2. содержит только буквы и цифры и "-", ".", "_"
//            3. длинна домена должна быть длиннее 1 буквы
//            4. домен может включать буквы цифры "-", ".", "_","^"
    private final boolean validateMail(String mail) {

        Pattern pattern = Pattern.compile(VALIDATE_MAIL);
        Matcher matcher = pattern.matcher(mail);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    /**
     * <p>This method validates the phone;It should be in format 3 number then symbol "-" then 2 numbers and symbol "-",
     * and then 2 more numbers</p>
     *
     * @return false if  it's wrong format of phone
     */
    //111-11-11
    private final boolean validatePhone(String phone) {

        Pattern pattern = Pattern.compile(VALIDATE_PHONE);
        Matcher matcher = pattern.matcher(phone);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }
}
