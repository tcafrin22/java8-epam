package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.FacultativeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.entity.Facultative;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Vlad on 09.11.2015.
 */

/**
 * <p>This class designed for processing  show fac command</p>
 *
 * @author Vlad
 */
public class ShowFacultativeService implements IService {
    private static final String FACULTATIVES = "facultatives";

    private static final ShowFacultativeService instance = new ShowFacultativeService();

    public static ShowFacultativeService getInstance() {
        return instance;
    }


    private static boolean flag = false;
    private static final String FACULTATIVE_FLAG = "facutatives";
    private static final String STATUS = "status";
    /**
     * <p>This method calls the appropriate DAO - SQLFacultativeDAO and it's method showAllFacultative;
     * Also sets attribute facultatives into request</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {

        if(request.getSession().getAttribute(STATUS) == null){
            throw new ServiceException("You are already logouted, please log in");
        }

        flag = !flag;
        request.setAttribute(FACULTATIVE_FLAG, flag);
        if (!flag) {
            return;
        }

        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        FacultativeDAO facultativeDAO = daoFactory.getFacultativeDAO();
        try {
            List<Facultative> facultatives = facultativeDAO.showAllFacultative();
            Object obj = facultatives;
            request.setAttribute(FACULTATIVES, obj);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }
}
