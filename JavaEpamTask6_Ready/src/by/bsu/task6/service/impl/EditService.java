package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.FacultativeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 17.11.2015.
 */

/**
 * <p>This class designed for processing edit command</p>
 *
 * @author Vlad
 */
public class EditService implements IService {
    private static final String FACULTATIVE_ID = "facid";
    private static final String MAXSTUDENTS = "maxstudents";
    private static final String TEACHER_ID = "id";
    private static final String FACULTATIVE_NAME = "facname";

    private static final EditService instance = new EditService();

    public static EditService getInstance() {
        return instance;
    }
    private static final String STATUS = "status";
    /**
     * <p>This method calls the appropriate DAO - SQLFacultativeDAO and it's method editFacultative</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        if(request.getSession().getAttribute(STATUS) == null){
            throw new ServiceException("You are already logouted, please log in");
        }

        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        FacultativeDAO facultativeDAO = daoFactory.getFacultativeDAO();
        try {
            int facultativeId = Integer.parseInt(request.getParameter(FACULTATIVE_ID));
            int maxStudents = Integer.parseInt(request.getParameter(MAXSTUDENTS));
            int teacherId = Integer.parseInt(request.getSession().getAttribute(TEACHER_ID).toString());
            String name = request.getParameter(FACULTATIVE_NAME);
            facultativeDAO.editFacultative(facultativeId, maxStudents, teacherId, name);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
