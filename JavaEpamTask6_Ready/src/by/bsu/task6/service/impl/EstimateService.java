package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.ResultDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This class designed for processing  estimate command</p>
 *
 * @author Vlad
 */
public class EstimateService implements IService {
    private static final String FACULTATIVE_ID = "facid";
    private static final String STUDENT_ID = "studid";
    private static final String MARK = "mark";
    private static final String COMMENT = "comment";
    private static final String FACULTATIVE_NAME = "facname";

    private static final EstimateService instance = new EstimateService();

    public static EstimateService getInstance() {
        return instance;
    }

    private static final int MIN_MARK = 1;
    private static final int MAX_MARK = 10;

    /**
     * <p>This method calls the appropriate DAO - SQLResultDAO and it's method addResult if the validation
     * was true</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        ResultDAO resultDAO = daoFactory.getResultDAO();
        try {
            int facultativeId = Integer.parseInt(request.getParameter(FACULTATIVE_ID));
            int studentId = Integer.parseInt(request.getParameter(STUDENT_ID));
            int mark = Integer.parseInt(request.getParameter(MARK));
            String facultativeName = request.getParameter(FACULTATIVE_NAME);
            if (!validate(mark)) {
                throw new ServiceException("Mark can't be lower then 1 and bigger then 10");
            }
            String comment = request.getParameter(COMMENT);
            resultDAO.addResult(facultativeId, studentId, mark, comment, facultativeName);

        } catch (DAOException | NumberFormatException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * <p>this method checks value of mark</p>
     *
     * @param mark mark for estimation
     */
    private final boolean validate(int mark) {
        if (mark < MIN_MARK || mark > MAX_MARK) {
            return false;
        }
        return true;
    }
}
