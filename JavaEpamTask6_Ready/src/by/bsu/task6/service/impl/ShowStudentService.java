package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.SubscribeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.entity.StudentWithMark;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This class designed for processing  show student command</p>
 *
 * @author Vlad
 */
public class ShowStudentService implements IService {
    private static final String STUDENTS = "students";
    private static final String ID = "id";
    private static final ShowStudentService instance = new ShowStudentService();

    public static ShowStudentService getInstance() {
        return instance;
    }

    private static final String TEACHER_ID = "teachid";

    private static final String STUDENT_FLAG = "stud_flag";
    private static final String FACULTATIVE_NAME = "facname";
    private static final String STATUS = "status";

    /**
     * <p>This method calls the appropriate SQLSubscribeDAOSubDAO and it's method showStud;
     * Also validates if this teacher can look on this info;
     * Sets attributes facultative name, stud flag, students, id into the request</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        if(request.getSession().getAttribute(STATUS) == null){
            throw new ServiceException("You are already logouted, please log in");
        }

        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        SubscribeDAO subscribeDAO = daoFactory.getSubDAO();

        try {
            int facultativeId = Integer.parseInt(request.getParameter(ID));
            int teacherIdWhoLead = Integer.parseInt(request.getParameter(TEACHER_ID));
            int teacherIdMy = Integer.parseInt(request.getSession().getAttribute(ID).toString());

            if (!validate(teacherIdWhoLead, teacherIdMy)) {
                throw new ServiceException("I can't download list of students of this fac, cause  i do not lead it");
            }

            List<StudentWithMark> students = subscribeDAO.showStud(facultativeId);
            Object obj = students;

            request.setAttribute(STUDENTS, obj);
            request.setAttribute(FACULTATIVE_NAME, request.getParameter(FACULTATIVE_NAME));
            request.setAttribute(STUDENT_FLAG, true);
            request.setAttribute(ID, request.getParameter(ID));


        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    private final boolean validate(int teachIdWhoLead, int teachIdMY) {
        if (teachIdWhoLead == teachIdMY) {
            return true;
        }
        return false;
    }
}
