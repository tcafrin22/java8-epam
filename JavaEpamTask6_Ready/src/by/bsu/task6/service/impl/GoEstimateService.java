package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.ResultDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 22.11.2015.
 */

/**
 * <p>This class designed for processing  go estimate command</p>
 *
 * @author Vlad
 */
public class GoEstimateService implements IService {
    private static final String FACULTATIVE_ID = "facid";
    private static final String STUDENT_ID = "studid";

    private static final GoEstimateService instance = new GoEstimateService();

    public static GoEstimateService getInstance() {
        return instance;
    }

    /**
     * <p>This method calls the appropriate DAO - SQLResultDAO and it's method validateOnAddResult
     * for validating; It checks can we use Estimate command</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        ResultDAO resultDAO = daoFactory.getResultDAO();
        try {
            int facultativeId = Integer.parseInt(request.getParameter(FACULTATIVE_ID));
            int studentId = Integer.parseInt(request.getParameter(STUDENT_ID));
            if (!resultDAO.validateOnAddResult(facultativeId, studentId)) {
                throw new ServiceException("exception in go estimate");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
