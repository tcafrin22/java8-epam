package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.ResultDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.entity.Result;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Vlad on 20.11.2015.
 */

/**
 * <p>This class designed for processing  show mark command</p>
 *
 * @author Vlad
 */
public class ShowMarkService implements IService {
    private static final ShowMarkService instance = new ShowMarkService();

    public static ShowMarkService getInstance() {
        return instance;
    }

    private static final String MARK_FLAG = "mark_flag";
    private static boolean flag = false;

    private static final String ID = "id";

    private static final String RESULTS = "results";
    private static final String STATUS = "status";
    /**
     * <p>This method calls the appropriate DAO - SQLResultDAO and it's method showResult;
     * Also sets attribute results into request</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        if(request.getSession().getAttribute(STATUS) == null){
            throw new ServiceException("You are already logouted, please log in");
        }
        flag = !flag;
        request.setAttribute(MARK_FLAG, flag);
        if (!flag) {
            return;
        }

        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        ResultDAO resultDAO = daoFactory.getResultDAO();
        try {
            int studentId = Integer.parseInt(request.getSession().getAttribute(ID).toString());
            List<Result> results = resultDAO.showResult(studentId);
            Object obj = results;

            request.setAttribute(RESULTS, obj);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
