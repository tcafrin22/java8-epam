package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.FacultativeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.resource.MessageManager;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 19.11.2015.
 */

/**
 * <p>This class designed for processing go edit command</p>
 *
 * @author Vlad
 */
public class GoEditService implements IService {
    private static final String ERROR_GO_EDIT_MESSAGE = "wrongID";
    private static final String MESSAGE_ID_ERROR = "message.wrongid";
    private static final String ID = "id";

    private static final GoEditService instance = new GoEditService();

    public static GoEditService getInstance() {
        return instance;
    }

    /**
     * <p>This method calls the appropriate DAO - SQLFacultativeDAO and it's method validateOnEdit
     * for validating; It checks can we use Edit command</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        FacultativeDAO facultativeDAO = daoFactory.getFacultativeDAO();
        try {
            int facultativeId = Integer.parseInt(request.getParameter(ID));
            int teacherId = Integer.parseInt(request.getSession().getAttribute(ID).toString());
            if (!facultativeDAO.validateOnEdit(teacherId, facultativeId)) {
                request.setAttribute(ERROR_GO_EDIT_MESSAGE,
                        MessageManager.getProperty(MESSAGE_ID_ERROR));
                throw new ServiceException("You can not edit an facultative ,the head of which you are not");
            }

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
