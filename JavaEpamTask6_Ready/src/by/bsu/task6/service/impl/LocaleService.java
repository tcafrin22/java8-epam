package by.bsu.task6.service.impl;

import by.bsu.task6.resource.MessageManager;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Vlad on 09.11.2015.
 */

/**
 * <p>This class designed for processing  locale  command</p>
 *
 * @author Vlad
 */
public class LocaleService implements IService {
    private static final String LANG = "lang";
    private static final String MESSAGE = "message";
    private static final LocaleService instance = new LocaleService();

    public static LocaleService getInstance() {
        return instance;
    }

    /**
     * <p>This method sets locale of our web-app</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        String lang = request.getParameter(LANG);
        MessageManager.setResourceBundle(ResourceBundle.getBundle(MESSAGE, new Locale(lang, lang.toUpperCase())));
        HttpSession session = request.getSession();
        session.setAttribute(LANG, lang);
    }
}
