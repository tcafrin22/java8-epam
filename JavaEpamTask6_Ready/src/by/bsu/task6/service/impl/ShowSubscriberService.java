package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.FacultativeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Vlad on 20.11.2015.
 */

/**
 * <p>This class designed for processing  show sub command</p>
 *
 * @author Vlad
 */
public class ShowSubscriberService implements IService {
    private static final ShowSubscriberService instance = new ShowSubscriberService();

    public static ShowSubscriberService getInstance() {
        return instance;
    }

    private static final String ID = "id";
    private static final String FACULTATIVE_NAMES = "facnames";
    private static final String SUB_FLAG = "sub_flag";
    private static boolean flag = false;
    private static final String STATUS = "status";

    /**
     * <p>This method calls the appropriate DAO - SQLResultDAO and it's method showSub;
     * Also sets attribute facultative name and sub flag into request</p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        if (request.getSession().getAttribute(STATUS) == null) {
            throw new ServiceException("You are already logouted, please log in");
        }
        flag = !flag;
        request.setAttribute(SUB_FLAG, flag);
        if (!flag) {
            return;
        }

        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        FacultativeDAO facultativeDAO = daoFactory.getFacultativeDAO();
        try {
            int studentId = Integer.parseInt(request.getSession().getAttribute(ID).toString());
            List<String> facultativesNames = facultativeDAO.showSub(studentId);
            Object obj = facultativesNames;
            request.setAttribute(FACULTATIVE_NAMES, obj);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }
}
