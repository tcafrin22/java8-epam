package by.bsu.task6.service.impl;

import by.bsu.task6.dao.DAOFactory;
import by.bsu.task6.dao.FacultativeDAO;
import by.bsu.task6.dao.exception.DAOException;
import by.bsu.task6.resource.MessageManager;
import by.bsu.task6.service.IService;
import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 18.11.2015.
 */

/**
 * <p>This class designed for processing delete facultative command</p>
 *
 * @author Vlad
 */
public class DeleteService implements IService {
    private static final String ERROR_DEL_MESSAGE = "wrongID";
    private static final String MESSAGE_ID_ERROR = "message.wrongid";

    private static final String ID = "id";
    private static final DeleteService instance = new DeleteService();

    public static DeleteService getInstance() {
        return instance;
    }

    /**
     * <p>This method calls the appropriate DAO - SQLFacultativeDAO and it's method deleteFacultative
     * if the validation correct </p>
     *
     * @param request A request that comes to this service
     * @throws by.bsu.task6.service.exception.ServiceException if he catches DAOException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {

        DAOFactory daoFactory = DAOFactory.getDAOFactory();
        FacultativeDAO facultativeDAO = daoFactory.getFacultativeDAO();
        try {
            String str = request.getSession().getAttribute(ID).toString();
            int teacherId = Integer.parseInt(str);
            int facultativeId = Integer.parseInt(request.getParameter(ID));
            if (!facultativeDAO.validateOnDelete(facultativeId, teacherId)) {
                //проверяю могу ли я вообще трогать этот факультатив
                request.setAttribute(ERROR_DEL_MESSAGE,
                        MessageManager.getProperty(MESSAGE_ID_ERROR));
                throw new ServiceException("Exception in delete validation");
            }

            facultativeDAO.deleteFacultative(facultativeId);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }
}
