package by.bsu.task6.service;

import by.bsu.task6.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Vlad on 07.11.2015.
 */

/**
 * <p>This interface should be implemented by every Service </p>
 *
 * @author Vlad
 */
public interface IService {
    void doService(HttpServletRequest request) throws ServiceException;
}
