package by.bsu.task6.filter;


import javax.servlet.*;
import java.io.IOException;

/**
 * Created by Vlad on 12.11.2015.
 */

/**
 * <p>This class is our web-app Filter which sets encoding and Content Type</p>
 *
 * @author Vlad
 */
public class RequestFilter implements Filter {
    private String encoding;
    private String contentType;

    private static final String CHARACTER_ENCODING = "characterEncoding";
    private static final String CONTENT_TYPE = "contType";
  //  private String urlCheak;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter(CHARACTER_ENCODING);
        contentType = filterConfig.getInitParameter(CONTENT_TYPE);

      //  urlCheak = filterConfig.getInitParameter("url");

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
//        System.out.println(request.getParameter("status"));
//        HttpServletRequest request1=(HttpServletRequest) request;
//        System.out.println(request1.getSession(false).getAttribute("status"));
//        HttpServletResponse response1=(HttpServletResponse) response;
//        String url = request1.getServletPath();
//        System.out.println("URL="+url);
        //System.out.println("url.equals(urlCheak)="+url.equals(urlCheak));
//        System.out.println("request getsession:");
//        System.out.println("request1.getSession(false)==null? ="+request1.getSession(false).getAttribute("status")==null);

//        if (url.equals(urlCheak) && request1.getSession(false).getAttribute("status")==null){
//            response1.sendRedirect(request1.getContextPath() + "/login.jsp");
//        }
//        else {
            request.setCharacterEncoding(encoding);
            response.setCharacterEncoding(encoding);
            response.setContentType(contentType);
            filterChain.doFilter(request, response);
//        }
    }

    @Override
    public void destroy() {

    }
}

