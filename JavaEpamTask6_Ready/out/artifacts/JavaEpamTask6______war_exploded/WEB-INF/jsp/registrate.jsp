<%@ page import="java.util.ResourceBundle" %>
<%--
  Created by IntelliJ IDEA.
  User: Vlad
  Date: 03.11.2015
  Time: 14:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <title>Registration</title>

    <link rel="stylesheet" type="text/css" href="../../styles/style.css">
    <script type="text/javascript" src="/script/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="/script/script.js"></script>




</head>
<body>


<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="message" var="loc"/>

<div class="wrapper">
<div class="registrateInfo">

    <form action="controller">
        <input type="hidden" name="command" value="registration"/>

        <div class="header">
            <h1><fmt:message key="message.signup" bundle="${loc}"/></h1>
        </div>
        <div class="form formRegistrate">
        <div class="content regCont">
            <label for="mycheakboxteach"><fmt:message key="message.teacher" bundle="${loc}"/></label>
            <input name="status" id="mycheakboxteach" type="radio"/>

            <label for="mycheakboxstud" class="stud"><fmt:message key="message.student" bundle="${loc}"/></label>
            <input name="status" id="mycheakboxstud" type="radio"/>

            <input type="text" name="firstname" placeholder="<fmt:message key="message.frname" bundle="${loc}"/>"
                   onfocus="this.value=''"/>

            <input type="text" name="lastname" placeholder="<fmt:message key="message.lname" bundle="${loc}"/>"
                    onfocus="this.value=''"/>

            <input type="number" min="1" max="9999999" name="id" placeholder="<fmt:message key="message.id" bundle="${loc}"/>"
                   onfocus="this.value=''"/>

            <input type="text" name="login" placeholder="<fmt:message key="message.login" bundle="${loc}"/>"
                   onfocus="this.value=''"/>

            <input type="password" name="password" placeholder="<fmt:message key="message.password" bundle="${loc}"/>"
                    onfocus="this.value=''"/>

            <input type="password" id="teach" name="passwordForTeachers" placeholder="<fmt:message key="message.secret" bundle="${loc}"/>"
                    onfocus="this.value=''"/>
            <input type="text" id="mail" name="mail" placeholder="<fmt:message key="message.mail" bundle="${loc}"/>"
                    onfocus="this.value=''"/>
            <input type="text" id="telephone" name="telephone" placeholder="<fmt:message key="message.phone" bundle="${loc}"/>"
                    onfocus="this.value=''"/>

        </div>
            <div class="footer">
                <input type="submit" name="action" class="buttonLogin"
                       value="<fmt:message key="message.signup" bundle="${loc}"/>"/>

            <a href="log"><fmt:message key="message.back" bundle="${loc}"/></a>
            <br>
            ${errorRegPassMessage}
            <br>
            ${wrongMail}
            <br>
            ${wrongPhone}
            <br>
            </div>
            </div>


    </form>
</div>

<footer class="regFooter">
    <ctg:copyright/>
</footer>
</div>
</body>
</html>
