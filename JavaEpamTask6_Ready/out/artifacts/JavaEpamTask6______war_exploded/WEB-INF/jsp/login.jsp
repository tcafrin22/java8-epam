<%@ page import="java.util.ResourceBundle" %>
<%--
  Created by IntelliJ IDEA.
  User: Vlad
  Date: 03.11.2015
  Time: 14:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<html>
<head>
    <title>Log In</title>
    <link rel="stylesheet" type="text/css" href="../../styles/style.css">


</head>
<body>

<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="message" var="loc"/>

<div class="wrapper">
<div class="locals">

    <form action="controller" method="post">
        <input type="hidden" name="command" value="locale"/>
        <input type="hidden" name="page" value="/WEB-INF/jsp/login.jsp"/>
        <input type="hidden" name="lang" value="en"/>
        <input type="submit" class="buttonlang" name="command" value="English"/>
    </form>

    <form action="controller" method="post">
        <input type="hidden" name="command" value="locale"/>
        <input type="hidden" name="page" value="/WEB-INF/jsp/login.jsp"/>
        <input type="hidden" name="lang" value="ru"/>
        <input type="submit"  class="buttonlang" name="command" value="Russian"/>
    </form>

</div>


<div class="formInfo">

    <form action="controller" class="login-form" method="post">
        <input type="hidden" name="command" value="login"/>

        <div class="header">
            <h1><fmt:message key="message.system" bundle="${loc}"/></h1>
        </div>
        <div class="form">
        <div class="content">

            <input type="text" name="login" placeholder="<fmt:message key="message.login" bundle="${loc}"/>" class="input username" onfocus="this.value=''"/>
            <input type="password" name="password" placeholder="<fmt:message key="message.password" bundle="${loc}"/>" class="input password"
                   onfocus="this.value=''"/>


        </div>

        <div class="footer">
            <input type="submit" name="action" class="buttonLogin"
                   value=" <fmt:message key="message.loginsystem" bundle="${loc}"/>"/>
            <a href="registr"><fmt:message key="message.signup" bundle="${loc}"/></a>
            <br/>
            ${errorLoginPassMessage}
            <br/>
            ${wrongAction}
            <br/>
            ${nullPage}
            <br/>
        </div>
        </div>
            </form>
        </div>
        <footer>  
            <ctg:copyright/>
        </footer>
</div>
</body>
</html>
