<%@ page import="java.util.ResourceBundle" %>
<%--
  Created by IntelliJ IDEA.
  User: Vlad
  Date: 03.11.2015
  Time: 14:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<html>
<head>
    <title>Estimate student</title>
    <link rel="stylesheet" type="text/css" href="../../styles/style.css">
    <script type="text/javascript" src="/script/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="/script/script.js"></script>

</head>
<body class="estimate">

<c:if test="${status == null}">
    <jsp:forward page="login.jsp"/>
</c:if>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="message" var="loc"/>

<div class="wrapper">
    <div class="registrateInfo">

    <form action="controller">
        <input type="hidden" name="command" value="estimate"/>
        <input type="hidden" name="studid" value="${requestScope.studid}"/>
        <input type="hidden" name="facid" value="${requestScope.facid}"/>
        <input type="hidden" name="facname" value="${requestScope.facname}"/>

        <div class="header">
            <h1><fmt:message key="message.estimate" bundle="${loc}"/></h1>
        </div>
        <div class="form formEstimate">
        <div class="content">

            <input type="number" min="1" max="10" name="mark" placeholder="<fmt:message key="message.mark" bundle="${loc}"/>"
                   class="input username" onfocus="this.value=''"/>


            <input  type="text"  name="comment" placeholder="<fmt:message key="message.com" bundle="${loc}"/>"
                    onfocus="this.value=''">
                </input>

        </div>
        <div class="footer">
            <input type="submit" name="action" class="button"
                   value="<fmt:message key="message.estimate" bundle="${loc}"/>"/>
            <%--<a href="controller?command=logout"><fmt:message key="message.logout" bundle="${loc}"/></a>--%>

            <a href="main"><fmt:message key="message.back" bundle="${loc}"/></a>
        </div>

    </div>
    </form>
    </div>



<footer class="regFooter">
    <ctg:copyright/>
</footer>
</div>
</body>
</html>
