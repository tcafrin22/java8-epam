<%@ page import="java.util.ResourceBundle" %>
<%--
  Created by IntelliJ IDEA.
  User: Vlad
  Date: 03.11.2015
  Time: 14:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <title>Add facultative</title>
    <link rel="stylesheet" type="text/css" href="../../styles/style.css">
    <script type="text/javascript" src="/script/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="/script/script.js"></script>

</head>
<body>

<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="message" var="loc"/>


<div class="wrapper">
<div class="registrateInfo">
    <form action="controller">
        <input type="hidden" name="command" value="addfac"/>

        <div class="header">
            <h1><fmt:message key="message.addfac" bundle="${loc}"/></h1>
        </div>

        <div class="form formAdd">
        <div class="content editLabel">

            <input type="number" min="1" max="9999999" name="facid" placeholder="<fmt:message key="message.facid" bundle="${loc}"/>"
                   onfocus="this.value=''"/>

            <input type="number" min="5" max="30" name="maxstudents" placeholder="<fmt:message key="message.maxstud" bundle="${loc}"/>"
                   onfocus="this.value=''"/>

            <input type="text"  name="facname" placeholder="<fmt:message key="message.facname" bundle="${loc}"/>"
                   onfocus="this.value=''"/>

        </div>

        <div class="footer">
            <input type="submit" name="action" class="buttonAdd"
                   value="<fmt:message key="message.addfac" bundle="${loc}"/>"/>
                   <!-- <a href="controller?command=logout"><fmt:message key="message.logout" bundle="${loc}"/></a> -->
                <a href="main"><fmt:message key="message.back" bundle="${loc}"/></a>
                <br/>
                ${errorCreateFac}
                <br/>
        </div>

    </div>
    </form>
</div>


<footer class="regFooter">
    <ctg:copyright/>
</footer> 
</div>
</body>
</html>
