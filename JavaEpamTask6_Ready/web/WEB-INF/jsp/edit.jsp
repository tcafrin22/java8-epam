<%@ page import="java.util.ResourceBundle" %>
<%--
  Created by IntelliJ IDEA.
  User: Vlad
  Date: 03.11.2015
  Time: 14:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<html>
<head>
    <title>Edit facultative</title>

    <link rel="stylesheet" type="text/css" href="../../styles/style.css">
    <script type="text/javascript" src="/script/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="/script/script.js"></script>

</head>
<body>
<c:if test="${status == null}">
    <jsp:forward page="login.jsp"/>
</c:if>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="message" var="loc"/>


<div class="wrapper">
<div class="registrateInfo">
    <form action="controller">
        <input type="hidden" name="command" value="edit"/>

        <div class="header">
            <h1>EDIT FAC</h1>
        </div>
        <div class="form formEdit">
        <div class="content editLabel">
            <input type="hidden" min="1" max="9999999" name="facid" value="${requestScope.id}"/>
            <label>Facultative id</label>
            <br>
            <p>
            ${requestScope.id}
            </p>
            <br>
            <label>Maximum students </label>
            <input type="number" min="5" max="30" name="maxstudents" value="${requestScope.maxstudents}"
                  onfocus="this.value=''"/>

            <label>Facultative name</label>
            <input type="text"  name="facname" value="${requestScope.name}"
                   onfocus="this.value=''"/>

        </div>
        <div class="footer">
            <input type="submit" class="buttonLogin"  name="command" 
            value="<fmt:message key="message.editfac" bundle="${loc}"/>"/>

                         <a href="controller?command=logout"><fmt:message key="message.logout" bundle="${loc}"/></a>
                         <!-- <a href="main"><fmt:message key="message.back" bundle="${loc}"/></a> -->
        </div>
    </div>
    </form>
</div>

<footer class="regFooter">
    <ctg:copyright/>
</footer> 

</div>
</body>
</html>
