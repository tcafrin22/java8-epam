<%--<%@ page import="java.util.ResourceBundle" %>--%>
<%--
  Created by IntelliJ IDEA.
  User: Vlad
  Date: 03.11.2015
  Time: 10:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<html>
<head>
    <title>Welcome</title>
    <link rel="stylesheet" type="text/css" href="../../styles/style.css">

</head>
<body>
<div class="wrapperMain">

    <c:if test="${status == null}">
        <jsp:forward page="login.jsp"/>
    </c:if>
    <fmt:setLocale value="${lang}"/>
    <fmt:setBundle basename="message" var="loc"/>


    <div class="header">
        <h1>
            <fmt:message key="message.welcome" bundle="${loc}"/> ${user}.
            <fmt:message key="message.status" bundle="${loc}"/>${status}.
            ID=>${id}
        </h1>
    </div>

    <hr>
    <div class="contMain">

        <a href="controller?command=logout"> <fmt:message key="message.logout" bundle="${loc}"/></a>


        <form action="controller" method="post">
            <input type="hidden" name="command" value="showfacultative"/>
            <input type="hidden" name="page" value="/WEB-INF/jsp/main.jsp"/>
            <input type="submit" name="command" value="<fmt:message key="message.showfac" bundle="${loc}"/>"/>
        </form>

        <c:if test="${status == 'Teacher'}">
            <a href="addfac"><fmt:message key="message.addfac" bundle="${loc}"/></a>
        </c:if>
        <c:if test="${status == 'Student'}">

            <form action="controller" method="post">
                <input type="hidden" name="command" value="showmark"/>
                <input type="hidden" name="page" value="/WEB-INF/jsp/main.jsp"/>
                <input type="submit" name="command" value="<fmt:message key="message.showmark" bundle="${loc}"/>"/>
            </form>

            <form action="controller" method="post">
                <input type="hidden" name="command" value="showsub"/>
                <input type="hidden" name="page" value="/WEB-INF/jsp/main.jsp"/>
                <input type="submit" name="command" value="
    <fmt:message key="message.showsub" bundle="${loc}"/>"/>
            </form>

        </c:if>

        <c:if test="${requestScope.facutatives}">
            <table>
                <tr>
                    <th>
                        <fmt:message key="message.facid" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.maxstud" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.teachid" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.facname" bundle="${loc}"/>

                    </th>
                    <th>
                        <fmt:message key="message.redact" bundle="${loc}"/>

                    </th>
                </tr>

                <c:forEach var="facultative" items="${requestScope.facultatives}">

                    <tr>
                        <td>
                            <c:out value="${facultative.facultativeId}"/>
                        </td>
                        <td>
                            <c:out value="${facultative.maxStudents}"/>
                        </td>
                        <td>
                            <c:out value="${facultative.teacherId}"/>
                        </td>
                        <td>
                            <c:out value="${facultative.name}"/>
                        </td>
                        <td>
                            <c:if test="${status == 'Teacher'}">
                                <!--Кнопка редактирования ,перенаправит меня на страницу с формами -->
                                <c:choose>
                                    <c:when test="${facultative.teacherId == id}">
                                        <form action="controller" method="post">
                                            <input type="hidden" name="command" value="goedit"/>

                                            <input type="hidden" name="id" value="${facultative.facultativeId}"/>
                                            <input type="hidden" name="maxstudents" value="${facultative.maxStudents}"/>
                                            <input type="hidden" name="name" value="${facultative.name}"/>
                                            <input type="submit" name="command"
                                                   value="<fmt:message key="message.editfac" bundle="${loc}"/>"/>
                                        </form>


                                        <form action="controller" method="post">
                                            <input type="hidden" name="command" value="delete"/>
                                            <input type="hidden" name="page" value="/WEB-INF/jsp/main.jsp"/>
                                            <input type="hidden" name="id" value="${facultative.facultativeId}"/>
                                            <input type="submit" name="command"
                                                   value="<fmt:message key="message.deletefac" bundle="${loc}"/>"/>
                                        </form>

                                        <form action="controller" method="post">
                                            <input type="hidden" name="command" value="showstudents"/>
                                            <input type="hidden" name="id" value="${facultative.facultativeId}"/>
                                            <input type="hidden" name="teachid" value="${facultative.teacherId}"/>
                                            <input type="hidden" name="facname" value="${facultative.name}"/>
                                            <input type="hidden" name="page" value="/WEB-INF/jsp/main.jsp"/>
                                            <input type="submit" name="command"
                                                   value="<fmt:message key="message.showstud" bundle="${loc}"/>"/>
                                        </form>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="message.nopermit" bundle="${loc}"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <c:if test="${status == 'Student'}">
                                <form action="controller" method="post">
                                    <input type="hidden" name="command" value="subscribe"/>
                                    <input type="hidden" name="id" value="${facultative.facultativeId}"/>
                                    <input type="hidden" name="maxstudents" value="${facultative.maxStudents}"/>
                                    <input type="submit" name="command"
                                           value="<fmt:message key="message.subscribe" bundle="${loc}"/>"/>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>


        <c:if test="${requestScope.stud_flag}">
            <table>
                <tr>
                    <th>
                        <fmt:message key="message.studid" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.frname" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.lname" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.phone" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.mark" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.redact" bundle="${loc}"/>
                    </th>

                </tr>


                <c:forEach var="student" items="${requestScope.students}">

                    <tr>
                        <td>
                            <c:out value="${student.student.studentId}"/>
                        </td>
                        <td>
                            <c:out value="${student.student.firstname}"/>
                        </td>
                        <td>
                            <c:out value="${student.student.lastname}"/>
                        </td>
                        <td>
                            <c:out value="${student.student.phone}"/>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${student.mark != 0}">
                                    <c:out value="${student.mark}"/>
                                </c:when>
                                <c:otherwise>
                                    -
                                </c:otherwise>
                            </c:choose>
                        </td>

                        <td>
                            <c:choose>
                            <c:when test="${student.mark == 0}">
                            <form action="controller" method="post">
                                <input type="hidden" name="command" value="goestimate"/>
                                <input type="hidden" name="studid" value="${student.student.studentId}"/>
                                <input type="hidden" name="facid" value="${requestScope.id}"/>
                                <input type="hidden" name="facname" value="${requestScope.facname}"/>
                                <input type="submit" name="command"
                                       value="<fmt:message key="message.estimate" bundle="${loc}"/>"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="message.havemark" bundle="${loc}"/>
                                </c:otherwise>
                                </c:choose>
                            </form>

                        </td>
                    </tr>

                </c:forEach>
            </table>
        </c:if>

        <c:if test="${requestScope.mark_flag}">
            <table>
                <tr>
                    <th>
                        <fmt:message key="message.facname" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.mark" bundle="${loc}"/>
                    </th>
                    <th>
                        <fmt:message key="message.com" bundle="${loc}"/>
                    </th>

                </tr>

                <c:forEach var="result" items="${requestScope.results}">

                    <tr>
                        <td>
                            <c:out value="${result.facultativeName}"/>
                        </td>
                        <td>
                            <c:out value="${result.mark}"/>
                        </td>
                        <td>
                            <c:out value="${result.comment}"/>
                        </td>
                    </tr>
                </c:forEach>


            </table>

        </c:if>

        <c:if test="${requestScope.sub_flag}">
            <table>
                <tr>
                    <th>
                        <fmt:message key="message.facnamesub" bundle="${loc}"/>
                    </th>
                </tr>
                <c:forEach var="facname" items="${requestScope.facnames}">
                    <tr>
                        <td align="center">
                            <c:out value="${facname}"/>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>

        <br/>
        <br/>
        ${wrongID}
        <br/>
        <br/>
        ${wrongIDteacher}
        <br/>
        <br/>
        ${wrongAction}
        <br/>
        <br/>
        ${errorEstimate}
        <br/>
        <br/>
        ${noSpace}
        <br/>
    </div>
    <footer class="regFooter">
        <ctg:copyright/>
    </footer>
</div>
</body>
</html>
